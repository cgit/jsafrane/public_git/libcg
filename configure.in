#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.
# 
#  Copyright International Business Machines Corp. 2008
# 
#  Authors:	Balbir Singh <balbir@linux.vnet.ibm.com>
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of version 2.1 of the GNU Lesser General Public License
#  as published by the Free Software Foundation.
# 
#  This program is distributed in the hope that it would be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
AC_PREREQ(2.61)

# In following section update all occurences of version, including soname
AC_INIT([libcgroup], 0.34,
	[http://sourceforge.net/tracker/?group_id=218421&atid=1043649])
AM_INIT_AUTOMAKE([foreign dist-bzip2])
# set library version, soname is libcgroup.so.MAJOR
AC_SUBST(LIBRARY_VERSION_MAJOR, 1)
AC_SUBST(LIBRARY_VERSION_MINOR, 0)
AC_SUBST(LIBRARY_VERSION_RELEASE, 34)

# we do not want static libraries
AC_DISABLE_STATIC

AC_CONFIG_SRCDIR([src])
AC_CONFIG_HEADER([config.h])

# Process command line options
AC_ARG_ENABLE([debug],
	[AC_HELP_STRING([--enable-debug],
		[enable extra debugging output [default=no]])],
	[AC_DEFINE([CGROUP_DEBUG], [],
		[Define to enable extra debugging output.])],
	[])

# Checks for programs.
AC_PROG_CXX
AC_PROG_CC
AC_PROG_YACC
if test "$YACC" != byacc; then
	YACC="$SHELL $missing_dir/missing byacc"
	AC_SUBST(YACC, "byacc")
fi
AM_PROG_LEX
AC_PROG_LIBTOOL

# Checks for libraries.
AC_CHECK_LIB([cgroup], [cgroup_create_cgroup])

# Checks for header files.
AC_HEADER_DIRENT
AC_HEADER_STDC
AC_CHECK_HEADERS([limits.h mntent.h stdlib.h string.h sys/mount.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_HEADER_STDBOOL
AC_C_CONST
AC_TYPE_UID_T
AC_C_INLINE
AC_TYPE_INT64_T
AC_TYPE_PID_T

# Checks for library functions.
AC_FUNC_CHOWN
AC_FUNC_GETMNTENT
AC_FUNC_MALLOC
AC_FUNC_REALLOC
AC_FUNC_STAT
AC_CHECK_FUNCS([getmntent hasmntopt memset mkdir rmdir strdup])

AC_CONFIG_FILES([Makefile
	tests/Makefile
	src/Makefile
	src/daemon/Makefile
	src/tools/Makefile
	src/pam/Makefile
	scripts/Makefile
	samples/Makefile
	include/Makefile
	doc/Makefile
	doc/man/Makefile
	dist/Makefile])
AC_CONFIG_FILES([dist/libcgroup.spec:dist/libcgroup.spec.in])
CFLAGS="$CFLAGS -Wall"
AC_OUTPUT

